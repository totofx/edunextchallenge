from django.conf.urls import url
from edunext_client import views

urlpatterns = [
    url(r'^list_client/$', views.ClientEdunext.as_view(), name='edunext_client'),
    url(r'^edit_client/(?P<uuid>[-\w]+)/$', views.EditEdunext.as_view(), name='edunext_edit'),
]
