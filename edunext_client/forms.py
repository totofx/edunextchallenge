from django import forms


class ClientForm(forms.Form):
    id = forms.CharField(max_length=400)
    ENABLE_MKTG_SITE = forms.BooleanField(required=False)
    LANGUAGE_CODE = forms.CharField(max_length=400)
    PLATFORM_NAME = forms.CharField(max_length=400)
    SITE_NAME = forms.CharField(max_length=400)
    TIME_ZONE_DISPLAYED_FOR_DEADLINES = forms.CharField(max_length=400)
    LAST_ACCESSED = forms.CharField(max_length=400)
    course_about_show_social_links = forms.BooleanField(required=False)
    course_index_overlay_logo_file = forms.CharField(max_length=400)
    course_org_filter = forms.CharField(max_length=400)
    released_languages = forms.CharField(max_length=400)
    show_homepage_promo_video = forms.BooleanField(required=False)
    show_partners = forms.BooleanField(required=False)
