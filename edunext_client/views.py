# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

import requests
# Create your views here.
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from edunext_client.forms import ClientForm


class ClientEdunext(TemplateView):

    template_name = 'edunext_client/list_client.html'

    def get_context_data(self, **kwargs):
        response = requests.get('http://localhost:8010/api/v1/customerdata/')
        data = json.loads(response.text)
        context_data = super(ClientEdunext, self).get_context_data(**kwargs)
        context_data['customer_data_list'] = data.get('results')
        return context_data


class EditEdunext(TemplateView):

    template_name = 'edunext_client/edit_client.html'

    def get_context_data(self, **kwargs):
        response = requests.get('http://localhost:8010/api/v1/customerdata/{}'.format(self.kwargs['uuid']))
        customer_data = json.loads(response.text)
        context_data = super(EditEdunext, self).get_context_data(**kwargs)
        context_data['customer_data'] = customer_data
        customer_data.get('data')['id'] = customer_data.get('id')
        print(customer_data.get('data'))
        context_data['form'] = ClientForm(initial=customer_data.get('data'))
        return context_data
