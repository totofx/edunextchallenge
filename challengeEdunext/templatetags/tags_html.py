from django import template

register = template.Library()


@register.filter
def addclass(field, css):
    field.field.widget.attrs['class'] = field.field.widget.attrs.get('class', '') + ' ' + css
    return field

